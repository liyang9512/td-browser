# TD Browser
<a href='https://gitee.com/liyang9512/td-browser/stargazers'><img src='https://gitee.com/liyang9512/td-browser/badge/star.svg?theme=dark' alt='star'></img></a>
<a href='https://gitee.com/liyang9512/td-browser/members'><img src='https://gitee.com/liyang9512/td-browser/badge/fork.svg?theme=dark' alt='fork'></img></a>

TD Browser是一款TDengine3.x客户端工具。支持执行任意SQL脚本、记录查询历史、导出查询结果；支持查看集群信息；支持管理连接、用户、数据库增删；支持查看超级表、普通表及子表数据。


![查询](https://foruda.gitee.com/images/1693890385194411499/42e22d40_5207497.png)

## 使用方式

下载对应操作系统的发行版安装文件，双击安装后即可使用。

## 技术栈

Electron + Vue3 + TypeScript + Vite + Nodejs

## 源码运行

#### 安装依赖
```bash
$ npm install
```
#### 本地运行
```bash
$ npm run dev
```
#### 源码打包
```bash
# windows安装包
$ npm run build:win

# macOS安装包
$ npm run build:mac

# Linux安装包
$ npm run build:linux
```
