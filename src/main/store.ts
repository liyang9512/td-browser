// @ts-ignore
import IpcMain = Electron.IpcMain

const Store = require('electron-store')
Store.initRenderer();
const store = new Store()

export function register(ipcMain: IpcMain) {
  // @ts-ignore
  ipcMain.on('setValue', function (event, key, value) {
    store.set(key, value)
  })
  // @ts-ignore
  ipcMain.handle('getValue', function (event, key, defaultValue): Promise<any> {
    return new Promise<any>((resolve) => {
      resolve(store.get(key, defaultValue))
    })
  })
  // @ts-ignore
  ipcMain.on('deleteValue', function (event, key) {
    store.delete(key)
  })
}
