const { ipcRenderer } = window.electron
/**
 * 导出csv
 */
export async function exportToCsv(dataList: any[]): Promise<boolean> {
  return ipcRenderer.invoke('exportToCsv', JSON.stringify(dataList))
}
