const { ipcRenderer } = window.electron

/**
 * 保存连接列表
 */
export function saveConnectionList(connectionList: any) {
  ipcRenderer.send('setValue', 'connectionList', JSON.stringify(connectionList))
}

/**
 * 获取链接列表
 */
export async function getConnectionList() {
  return getList('connectionList')
}

/**
 * 保存查询历史列表
 */
export function saveQueryHistoryList(queryHistoryList: any) {
  ipcRenderer.send('setValue', 'queryHistoryList', JSON.stringify(queryHistoryList))
}

/**
 * 获取查询历史列表
 */
export async function getQueryHistoryList() {
  return getList('queryHistoryList')
}

async function getList(key: string) {
  return new Promise<[]>((resolve) => {
    let jsonStr = '[]'
    ipcRenderer
      .invoke('getValue', key, '[]')
      .then((result: any) => {
        jsonStr = result
      })
      .finally(() => {
        resolve(JSON.parse(jsonStr))
      })
  })
}
