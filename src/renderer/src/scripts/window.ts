const { ipcRenderer } = window.electron

export function minimizeWin() {
  ipcRenderer.send("min");
}
export function maximizeWin() {
  ipcRenderer.send("max");
}

export function restoreWin() {
  ipcRenderer.send("restore");
}
export function closeWin() {
  ipcRenderer.send("close");
}
