export class Connection {
  name: string
  host: string
  port: string
  username: string
  password: string
  version: string | undefined
  constructor(
    name: string,
    host: string,
    port: string,
    username: string,
    password: string,
    version: string | undefined
  ) {
    this.name = name
    this.host = host
    this.port = port
    this.username = username
    this.password = password
    this.version = version
  }
}

export class Meta {
  columnName?: string
  typeName?: string
  size?: number
}

export class QueryResult {
  success: boolean
  message: string | undefined
  meta: Array<Meta> | undefined
  data: Array<Array<any>> | undefined

  constructor(success: boolean) {
    this.success = success
  }
}
