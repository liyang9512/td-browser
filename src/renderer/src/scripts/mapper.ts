import type { Connection } from './type'
import { QueryResult } from './type'

const { ipcRenderer } = window.electron

/**
 * 切换连接
 */
export async function changeConnection(connection: Connection): Promise<boolean> {
  return ipcRenderer.invoke('changeConnection', JSON.stringify(connection))
}

/**
 * 切换数据库
 */
export async function changeDatabase(database: string): Promise<boolean> {
  return ipcRenderer.invoke('changeDatabase', database)
}

/**
 * 查询当前连接
 */
export async function getCurrConnection(): Promise<string> {
  return ipcRenderer.invoke('getCurrConnection')
}

/**
 * 查询当前数据库
 */
export async function getCurrDatabase(): Promise<string> {
  return ipcRenderer.invoke('getCurrDatabase')
}

/**
 * 执行查询
 */
export async function doQuery(sql: string): Promise<QueryResult> {
  return ipcRenderer.invoke('doQuery', sql)
}
