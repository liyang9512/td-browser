import {createRouter, createWebHashHistory} from 'vue-router'

import HomeLayout from '../views/layout/HomeLayout.vue'
import ConnectionLayout from '../views/layout/ConnectionLayout.vue'
import DatabaseLayout from '../views/layout/DatabaseLayout.vue'

import ConnectionView from '../views/connection/ConnectionView.vue'
import DatabaseView from '../views/database/DatabaseView.vue'
import QueryView from '../views/query/QueryView.vue'
import StableView from '../views/stable/StableView.vue'
import TableView from '../views/table/TableView.vue'
import ClusterView from '../views/cluster/ClusterView.vue'
import UserView from '../views/user/UserView.vue'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'HomeLayout',
      redirect: '/home/connection',
      component: HomeLayout,
      children: [
        {
          path: '/home/connection',
          name: 'ConnectionView',
          component: ConnectionView
        },
      ]
    },
    {
      path: '/connection',
      name: 'ConnectionLayout',
      redirect: '/connection/database',
      component: ConnectionLayout,
      children: [
        {
          path: '/connection/database',
          name: 'DatabaseView',
          component: DatabaseView
        },
        {
          path: '/connection/user',
          name: 'UserView',
          component: UserView
        },
        {
          path: '/connection/cluster',
          name: 'ClusterView',
          component: ClusterView
        }
      ]
    },
    {
      path: '/database',
      name: 'DatabaseLayout',
      redirect: '/database/query',
      component: DatabaseLayout,
      children: [
        {
          path: '/database/query',
          name: 'QueryView',
          component: QueryView
        },
        {
          path: '/database/stable',
          name: 'StableView',
          component: StableView
        },
        {
          path: '/database/table',
          name: 'TableView',
          component: TableView
        },
      ]
    },
  ]
})

export default router
